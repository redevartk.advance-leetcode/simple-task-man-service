
# [Advance Leetcode] Simple Task Manager

Simple task management, this is back-end service only

## Properties

* Programming Lang: PHP 8.2.3
* Framework: Laravel 10.32.1
* Database: MySQL 8.0.30
* API Tools: L5_Swagger
* Patterns: Repository, Service

## Installation / Dev Run

1. Clone repo
2. Install php in your system
3. Open the project
4. Adjust database in .env
5. Do "php artisan migrate"
6. Do "php artisan db:seed"
7. Run in dev by "php artisan serve"
