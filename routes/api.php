<?php

use App\Http\Controllers\TaskControllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth.api.key'])->group(function () {

    Route::prefix('get')->group(function () {
        Route::get('task', [TaskControllers::class, 'getAllTask'])->name('task.get');
    });

    Route::prefix('edit')->group(function () {
        Route::post('task/order', [TaskControllers::class, 'editTaskOrder'])->name('task.order.edit');
        Route::post('task/description', [TaskControllers::class, 'editTaskDescription'])->name('task.description.edit');
        Route::post('task/complete_at', [TaskControllers::class, 'markTaskAsComplete'])->name('task.complete_at.edit');
    });

    Route::prefix('create')->group(function () {
        Route::post('task', [TaskControllers::class, 'createNewTask'])->name('task.create');
    });

    Route::prefix('delete')->group(function () {
        Route::post('task', [TaskControllers::class, 'deleteTask'])->name('task.delete');
    });
});
