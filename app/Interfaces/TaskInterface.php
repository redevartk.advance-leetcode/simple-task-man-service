<?php

namespace App\Interfaces;

interface TaskInterface
{
    public function getAllTask();
    public function createNewTask($data);
    public function editTaskDescription($id, $description);
    public function deleteTask($id);
    public function editTaskOrder($id, $order);
    public function markTaskAsComplete($id);
}
