<?php

namespace App\Repositories;

use App\Interfaces\TaskInterface;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class TaskRepository implements TaskInterface{

    private $taskModel;

    public function __construct(Task $taskModel){
        $this->taskModel = $taskModel;
    }

    public function getAllTask(){
        $query = $this->taskModel->all();

        return $query;
    }

    public function createNewTask($data){
        $query = $this->taskModel->create([
            'description' => $data['description'],
            'complete_at' => $data['complete_at'],
            'order' => $data['order']
        ]);

        return $query;
    }

    public function editTaskDescription($id, $description){
        $query = $this->taskModel->where('id', $id)->update(['description' => $description]);

        return $query;
    }

    public function deleteTask($id) {
        // Convert a single ID to an array if $idOrArray is not already an array
        $id = is_array($id) ? $id : [$id];

        // Delete tasks where the 'id' column matches any value in the $ids array
        $query = $this->taskModel->whereIn('id', $id)->delete();

        return $query;
    }

    public function editTaskOrder($id, $order){
        $query = $this->taskModel->where('id', $id)->update(['order' => $order]);

        return $query;
    }

    public function markTaskAsComplete($id) {
        // Convert a single ID to an array if $idOrArray is not already an array
        $id = is_array($id) ? $id : [$id];

        // Get the current date and time
        $currentDateTime = Carbon::now();

        // Delete tasks where the 'id' column matches any value in the $ids array
        $query = $this->taskModel->whereIn('id', $id)->update(['complete_at' => $currentDateTime]);

        return $query;
    }
}
