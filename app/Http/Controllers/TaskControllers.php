<?php

namespace App\Http\Controllers;

use App\Repositories\TaskRepository;
use Illuminate\Http\Request;

use OpenApi\Annotations as OA;

class TaskControllers extends Controller
{
    private $taskRepository;

    public function __construct(TaskRepository $taskRepository){
        $this->taskRepository = $taskRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/get/task",
     *     tags={"task"},
     *     summary="Get all task data",
     *     description="-",
     *     operationId="get-all-task",
     *     security={{ "ApiKeyAuth": {} }},
     *     @OA\Response(
     *         response="default",
     *         description="will return all task data"
     *     )
     * )
     */

    public function getAllTask(){
        try {
            $result = $this->taskRepository->getAllTask();
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => 0, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/create/task",
     *     tags={"task"},
     *     summary="Create a new task",
     *     description="-",
     *     operationId="create-task",
     *     @OA\Parameter(
     *          name="description",
     *          description="description of the task",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     security={{ "ApiKeyAuth": {} }},
     *     @OA\Response(
     *         response="default",
     *         description="will return status 200 if success and if failed will return status 500"
     *     )
     * )
     */

    public function createNewTask(Request $request){
        try {
            $result = $this->taskRepository->createNewTask($request);
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => 0, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/edit/task/description",
     *     tags={"task"},
     *     summary="Edit task description",
     *     description="-",
     *     operationId="edit-task-description",
     *     @OA\Parameter(
     *          name="id",
     *          description="id of the task",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="description",
     *          description="description of the task",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     security={{ "ApiKeyAuth": {} }},
     *     @OA\Response(
     *         response="default",
     *         description="will return status 200 if success and if failed will return status 500"
     *     )
     * )
     */
    public function editTaskDescription(Request $request){
        try {
            $result = $this->taskRepository->editTaskDescription($request['id'], $request['description']);
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => 0, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/delete/task",
     *     tags={"task"},
     *     summary="Delete task",
     *     description="-",
     *     operationId="delete-task",
     *     @OA\Parameter(
     *          name="id",
     *          description="id of the task",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(type="integer"),
     *          )
     *     ),
     *     security={{ "ApiKeyAuth": {} }},
     *     @OA\Response(
     *         response="default",
     *         description="will return status 200 if success and if failed will return status 500"
     *     )
     * )
     */
    public function deleteTask(Request $request){
        try {
            $result = $this->taskRepository->deleteTask($request['id']);
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => 0, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/edit/task/order",
     *     tags={"task"},
     *     summary="Edit task order",
     *     description="-",
     *     operationId="edit-task-order",
     *     @OA\Parameter(
     *          name="id",
     *          description="id of the task",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="order",
     *          description="order of the task",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     security={{ "ApiKeyAuth": {} }},
     *     @OA\Response(
     *         response="default",
     *         description="will return status 200 if success and if failed will return status 500"
     *     )
     * )
     */
    public function editTaskOrder(Request $request){
        try {
            $result = $this->taskRepository->editTaskOrder($request['id'], $request['order']);
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => 0, 'message' => $th->getMessage()], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/edit/task/complete_at",
     *     tags={"task"},
     *     summary="Mark complete at task",
     *     description="-",
     *     operationId="mark-complete-task",
     *     @OA\Parameter(
     *          name="id",
     *          description="id of the task",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(type="integer"),
     *          )
     *     ),
     *     security={{ "ApiKeyAuth": {} }},
     *     @OA\Response(
     *         response="default",
     *         description="will return status 200 if success and if failed will return status 500"
     *     )
     * )
     */
    public function markTaskAsComplete(Request $request){
        try {
            $result = $this->taskRepository->markTaskAsComplete($request['id']);
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => 0, 'message' => $th->getMessage()], 500);
        }
    }
}
