<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $apiKey = config('app.api_key');

        $providedApiKey = $request->header('X-API-KEY'); // Assuming the API key is sent in a header

        if ($providedApiKey !== $apiKey) {
            return response()->json(['data'=> 0, 'message' => 'Unauthorized'], 401);
        }

        return $next($request);
    }
}
