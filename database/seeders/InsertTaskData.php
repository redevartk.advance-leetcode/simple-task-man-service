<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InsertTaskData extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Generate current timestamps
        $now = Carbon::now();

        // Generate sample Task records
        DB::table('task')->insert([
            [
                'description' => 'Sample Task 1',
                'order' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'description' => 'Sample Task 2',
                'order' => 2,
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'description' => 'Sample Task 3',
                'order' => 3,
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}
